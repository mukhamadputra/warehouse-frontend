import React, { useState } from "react"
import "./StockList.css"

const StockList = () => {
    const [materials, setMaterials] = useState([
        { mid: 'MTR01', mat_desc: 'Mobil Super 4T 20W50 800 ml', mat_in: '2022-02-12', mat_out: '2022-02-12', mat_rack: 'S-12A' },
        { mid: 'MTR02', mat_desc: 'Evalube Runner 20W40 800 ml', mat_in: '2022-02-12', mat_out: '2022-02-12', mat_rack: 'S-12A' },
        { mid: 'MTR03', mat_desc: 'Repsol Moto Super 4T SAE 20W50 800 ml', mat_in: '2022-02-12', mat_out: '2022-02-12', mat_rack: 'S-12A' }
    ])

    const [input,setInput]=useState({
        mid: '',
        mat_desc: '',
        mat_in: '',
        mat_out: '',
        mat_rack: ''
    })

    const handleChange = (e) =>{
        let name = e.target.name
        let typeOfValue = e.target.value
        setInput({...input, [name] : [typeOfValue]})
    }

    const handleSubmit = (e) =>{
        e.preventDefault()
        let mid = input.mid
        let mat_desc = input.mat_desc
        let mat_in = input.mat_in
        let mat_out = input.mat_out
        let mat_rack = input.mat_rack
        setMaterials([...materials, {
            mid,
            mat_desc,
            mat_in,
            mat_out,
            mat_rack
        }])
        setInput({
            mid: '',
            mat_desc: '',
            mat_in: '',
            mat_out: '',
            mat_rack: ''
        })
    }

    return (
        <>
            <h1>Warehouse Stock List</h1>
            <table>
                <thead>
                    <th>mid</th>
                    <th>mat_desc</th>
                    <th>mat_in</th>
                    <th>mat_out</th>
                    <th>mat_rack</th>
                </thead>
                <tbody>
                    {materials.map((r, index) => {
                        return (
                            <tr key={index}>
                                <td>{r.mid}</td>
                                <td>{r.mat_desc}</td>
                                <td>{r.mat_in}</td>
                                <td>{r.mat_out}</td>
                                <td>{r.mat_rack}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <br />
            <h1>Input Form</h1>
            <form onSubmit={handleSubmit}>
                <label htmlFor="mid">mid:</label>
                <input type="text" name="mid" value={input.mid} onChange={handleChange} /><br />
                <label htmlFor="mat_desc">mat_desc:</label>
                <input type="text" name="mat_desc" value={input.mat_desc} onChange={handleChange} /><br />
                <label htmlFor="mat_in">mat_in:</label>
                <input type="date" name="mat_in" value={input.mat_in} onChange={handleChange} /><br />
                <label htmlFor="mat_out">mat_out:</label>
                <input type="date" name="mat_out" value={input.mat_out} onChange={handleChange} /><br />
                <label htmlFor="mat_rack">mat_rack:</label>
                <input type="text" name="mat_rack" value={input.mat_rack} onChange={handleChange} />
                <br />
                <input type="submit" />
            </form>
        </>
    )
}

export default StockList